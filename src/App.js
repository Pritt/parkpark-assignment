import React, { Component } from 'react';
import ListComponent from './components/listComponent';
import StateComponent from './components/stateComponent';
import MarkerBlip from './components/MarkerBlip';
import Griddle, {plugins, RowDefinition, ColumnDefinition} from 'griddle-react';
import { connect } from 'react-redux';
import GoogleMapReact from 'google-map-react';
import './App.css';

class App extends Component {
  constructor(){
      super()
      this.state = {
          parkingData: [],
          selected: 'test',
          dateForTime: '',
          timeFrom: '00:00',
          timeto: '00:00'
      }
  }

  onValueSelected(e){
    console.log(e.currentTarget.value);
    this.setState({
      selected: e.currentTarget.value
    })
  }


  componentDidMount(){
    fetch('http://data.kk.dk/parking/latest/50')
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson.results)
          this.setState({parkingData : responseJson.results});

        })
        .catch((error) => {
          console.log(error)
        });
  }

  updateParkingData(){
    let date = new Date(this.state.dateForTime);
    let timeFrom = this.state.timeFrom;
    let timeTo = this.state.timeTo;

    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDate();
    
    let yearString = year + '/'+ month + '/' + day;
    let timeInterval = timeFrom + '-' + timeTo;
    try{
      /*data.kk.dk/parking/{år}/{måned}/{dag}/{time:minut}-{time:minut}*/
      fetch('http://data.kk.dk/parking/'+ yearString + '/' + timeInterval)
      .then((response) => response.json())
      .then((responseJson) =>{
        this.setState({parkingData : responseJson.results})
        console.log(responseJson);
      })
      .catch((error) => {
        console.log(error)
      })
    }
    catch(error){
      console.log(error);
    }
  }

  /*Well this is redundant*/
  updateDateForTime(dateForTime){
    this.setState({dateForTime: dateForTime.currentTarget.value});
  }
  /*Well this is redundant*/
  updateTimeFrom(e){
    this.setState({timeFrom: e.currentTarget.value});
  }
  /*Well this is redundant*/
  updateTimeTo(e){
    this.setState({timeTo: e.currentTarget.value});
  }
  


  render() {
    let dateFormat = ({value}) => <span>{new Date(value).toDateString()}</span>;
    let latLongFormat = ({value}) => {
    // Some lon and lat values are too long and thus disrupt the size of the container, making it look ugly
    // This will remove some
      if(value.length > 16){
        let longLatLon = value.split(',');
        longLatLon[0] = longLatLon[0].substring(0, 7);
        longLatLon[1] = longLatLon[1].substring(0, 7);
        value = longLatLon[0] + '/' + longLatLon[1]
      }
      return <span>{value}</span>
    };

    const TableBody = connect((state, props) => ({
      visibleData: plugins.LocalPlugin.selectors.visibleDataSelector(state)
        }))(({ rowIds, Row, visibleData }) => (
          <GoogleMapReact
            defaultCenter={{lat: 56.27, lng: 10.78}}
            defaultZoom={7}
          >
        { visibleData && visibleData.toJSON().map((r) => {
          let splitLonLat = [];
          if(r.sellingPointLocation.includes(',') || r.sellingPointLocation.includes('/')){
            if(r.sellingPointLocation.includes(',')){
              splitLonLat = r.sellingPointLocation.split(',');
            }
            else if(r.sellingPointLocation.includes('/')){
              splitLonLat = r.sellingPointLocation.split('/');
            }
          }
          else{
            splitLonLat[0] = "55.6764";
            splitLonLat[1] = "12.5681";
          }
          
          try{
            r.latitude = splitLonLat[0];
            r.longitude = splitLonLat[1];
          }
          catch(error){
            console.log(error);
          }
          return <Row key={r.prid} griddleKey={r.prid} lat={r.latitude} lng={r.longitude} {...r} />})
        }
      </GoogleMapReact>
    ));

    const CustomTableComponent = OriginalComponent => class CustomTableComponent extends Component {
      static contextTypes = {
        components: React.PropTypes.object
      }

      render() {
          return (
            <div style={{ height: 800}}>
              <this.context.components.TableBody />
            </div>
          );
      }
    }

    return (
      <div className="App">
        {/* Getting to know React
        <form>
        <input type="radio" name="test" onChange={this.onValueSelected.bind(this)} checked={this.state.selected === 'test'} value="test" /> test <br/>
        <input type="radio" name="test" onChange={this.onValueSelected.bind(this)} checked={this.state.selected === 'test2'} value="test2" /> test 2 <br/>
        <input type="radio" name="test" onChange={this.onValueSelected.bind(this)} checked={this.state.selected === 'test3'} value="test3" /> test 3 <br/>
        </form>
        <ListComponent name="test1"  selected={this.state.selected}/>
        <ListComponent name="test2" selected={this.state.selected}/>
        <ListComponent name="test3" selected={this.state.selected}/>
        <StateComponent />*/}

        {/* Outputting parking data
          <ListComponent  providers={this.state.parkingData}/>
        */}
        <div id="intervalSelector">
          <h3>Select the Date and time, from and to, to view results</h3>
          <label htmlFor="dateForTime">Date: </label>
          <input className="dateForTime" type="date" onChange={this.updateDateForTime.bind(this)} />
          <label htmlFor="timeFrom">Time From: </label>
          <input className="timeFrom" type="time"  onChange={this.updateTimeFrom.bind(this)} />
          <label htmlFor="timeTo">Time To: </label>
          <input className="timeTo" type="time" onChange={this.updateTimeTo.bind(this)} />
          <br/>
          <button id="submitButton" onClick={this.updateParkingData.bind(this)}>Get data within date and times</button>
        </div>
        

        <div id="tableContainer">
          <Griddle data={this.state.parkingData}
                  plugins={[plugins.LocalPlugin]}
                  >
            <RowDefinition>
              <ColumnDefinition id="providerId" title="Provider" />
              <ColumnDefinition id="validityBegin" title="Parking Start" customComponent={dateFormat} />
              <ColumnDefinition id="validityEnd" title="Parking End" customComponent={dateFormat} />
              <ColumnDefinition id="sellingPointLocation" title="Lat/Lon" customComponent={latLongFormat} />
            </RowDefinition>
          </Griddle>
        </div>

        <div className="providerCircle">
          <div className="Parkman"></div><p>Parkman</p>
          <div className="EasyPark"></div><p>EasyPark</p>
          <div className="Logos"></div><p>Logos</p>
        </div>
        <div className="griddleMap">
          <Griddle
            data={this.state.parkingData}
            plugings={[plugins.LocalPlugin]}
            pageProperties={{
              pageSize: 50000
            }}
            components={{
              TableContainer: CustomTableComponent,
              TableBody,
              Row: MarkerBlip,
            }}
          />
        </div>


      </div>
    );
  }
}

export default App;


/*
1. Users want to be able to see total parking in a period. Check parking between certain periods.
Implementation - Go date from and To (including time) using "data.kk.dk/parking/{år}/{måned}/{dag}/{time:minut}"
Date Selector FROM  = object.validityBegin
Date Selector TO    = object.validityEnd

2. Can see how many parking spaces are taken per parking company (provider)
Implementation - Sort by provider
A column with all the parking companies and count how many parkings they have within that result? Maybe?
object.providerId => count

Filter results to show only => object.transactionId

3. Can see the list of parking with privider, time and coordinates
implementation - make three columns, Provider, time(start and ending time?) and coordinates

Provider = object.providerId
Time - Is it just just validity hours? object.validityHours
coordinates = first normalize all the lon/lan to dash instead of comma. object.sellingPointLocation

4. Can filter list and/or card by provider
Implementation - filter to only show certain provider.
Dropdown filter => object.providerId

*/

/*
  @param from API
  
  areaId                  - COLOR
  areaManagerId           - KK - what could this be?
  lastModified            - DATE of last modified
  prid                    - Parking ID?
  productDescription      - Product Description foooor?
  providerId              - Provider Identifier: Provider name
  sellingPointId          - Selling point ID? not sure. Maybe same as "providerId". Might be what they used to buy the ticket?
  sellingPointLocation    - LATITUDE/LONGITUDE Coordinates of where vehicle is parked
  transactionId           - Identifier for the transaction
  validityBegin           - Parking validity STARTING DATE
  validityEnd             - Parking validity ENDING DATE
  validityHours           - Parking validity FROM-TO
  vehicleId               - Vehicle ID: Presumably the license plate?

  Seems like sellingPointLocation is either lon/lan or lon,lan
*/

/*
  Learning ReactJS
  Since I have very little, almost none, experience in ReactJS (aside from playing around with the examples in React Native previously...long ago)
  I started out by checking how to input and output within app component and then followed by input/output communication between two components.

  Managed to render the object as a list.

  Resources: https://github.com/brillout/awesome-react-components
  http://griddlegriddle.github.io/Griddle/ - This seems to be doing almost what the assignment wants.

  Seems like native browser datepicker is accepting new onChange value and holds on to previous one. No clue why. My test worked just fine to change and recieve latest radio button.
  Why is everyone creating their own date picker, googling seems to be not helping and leading to datepicker libraries.
  
  And that was 2 hours wasted when the actual value was being set but somehow didn't log properly.

  Integrated the map
  For some reason the initial load of the state, the map only loads 3 or 4 points. So maybe a delay?
  Once new data has been recieved with Date, time from and to, refresh shows a lot more points.
  
  
  @Bugs:
  Map does not load all points initially
  Slow loading.

  @Todo:
  Remove depricated functions
  Google maps reloads whenever date or times are changed, meaning onChange is affecting all of them. One-way binding causing problems? Possible fix: remove onChange and add getElementBy- value on button press.


  https://spring.io/guides/tutorials/react-and-spring-data-rest/

*/
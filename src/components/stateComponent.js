import React, { Component } from 'react';

class StateComponent extends Component {
  render() {
    return (
        <div className="stateThis">
            <p>Currently in this state! Trying to pass data between two components through state check</p>
        </div>
    );
  }
}

export default StateComponent;
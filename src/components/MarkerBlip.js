import React, { Component } from 'react';
class MarkerBlip extends Component {
    constructor(props) {
      super(props);
      this.state = { showInfo: false };
    }

    onMouseEnter = () => {
      this.setState({showInfo: true});
    }

    onMouseLeave = () => {
      this.setState({showInfo: false})
    }

    render() {
      return (
        <div>
          <div
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
            className={this.props.providerId}
            style={{
              borderRadius: 50,
              backgroundColor: "#512DA8",
              width:25,
              height: 25,
            }}
          />
          { this.state.showInfo &&
            <div
              style={{
                backgroundColor: "#EDEDED",
                border: "1px solid #777",
                width: 150,
                height: 70,
                padding: 10,
                position: 'relative',
                zIndex: 9999
              }}
            >
              <h4 style={{margin: 0}}>{this.props.providerId}</h4>
              <small>{this.props.sellingPointId}</small>
            </div>
          }
        </div>
      )
    }
  }
  export default MarkerBlip;